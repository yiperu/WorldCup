//
//  TeamDetailViewController.swift
//  WorldCup1
//
//  Created by Henry AT on 7/10/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import UIKit

class TeamDetailViewController: UIViewController {

    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var sigilImageView: UIImageView!
    @IBOutlet weak var wordLabel: UILabel!

    var model: Team
    
    init(model: Team) {
        self.model = model
        
//        super.init(nibName: "TeamDetailViewController", bundle: Bundle(for: TeamDetailViewController.self))
//        super.init(nibName: nil, bundle: nil)
        super.init(nibName: nil, bundle: Bundle(for: type(of: self)))
        
        title = model.name
    }
    
    
    // Esto es una tontera de Apple.
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        syncronizeModel()
        setUpUI()
    }

    func syncronizeModel() {
        self.teamNameLabel.text = "Team \(model.name)"
        self.sigilImageView.image = model.sigil.image
        self.wordLabel.text = model.word
        
//        title = model.name
    }


    func setUpUI()  {
        // TODO: Boton de navegador
        let wikiButton = UIBarButtonItem(title: "Wiki", style: .plain, target: self, action: #selector(displayWiki))
        let membersButton = UIBarButtonItem(title: "Members", style: .plain, target: self, action: #selector(displayMembers))
        
//        navigationItem.rightBarButtonItem = wikiButton
        navigationItem.rightBarButtonItems = [membersButton, wikiButton]
    }
    
    @objc func displayWiki() {
        // Aqui llamar al Controlador que muestra la pagina
        let wiki = WikiViewController(model: model)
        navigationController?.pushViewController(wiki, animated: true)
        
    }
    
    @objc func displayMembers()  {
        // TODO:
        let memberViewController = MembersTeamListTableViewController(model: self.model.sortedMembers())
        navigationController?.pushViewController(memberViewController, animated: true)
        
    }
}

extension TeamDetailViewController: TeamListTableViewControllerDelegate {
    
    func teamListViewController(_ viewController: TeamListTableViewController, didSelectTeam team: Team) {
        self.model = team
        syncronizeModel()
    }
    
    
}
