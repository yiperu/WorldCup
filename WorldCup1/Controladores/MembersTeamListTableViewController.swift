//
//  MembersTeamListTableViewController.swift
//  WorldCup1
//
//  Created by Henry AT on 9/25/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import UIKit

class MembersTeamListTableViewController: UITableViewController {
    
    let model: [Player]
    
    init(model: [Player]) {
        self.model = model
        super.init(style: .plain)
        title = "Members"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "cellId"
        
        let player = self.model[indexPath.row]
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
        }
        
        cell?.textLabel?.text = player.name
        
        return cell!
    }
    
}
