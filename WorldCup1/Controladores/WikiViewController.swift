//
//  WikiViewController.swift
//  WorldCup1
//
//  Created by Henry AT on 9/25/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import UIKit
import WebKit

class WikiViewController: UIViewController {

    
    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var model: Team
    
    init(model: Team) {
        self.model = model
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        syncronizeModel(model: self.model)
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        self.webView.navigationDelegate = self
        // Do any additional setup after loading the view.
    }

    func syncronizeModel(model: Team) {
        self.title = model.name
        self.webView.load(URLRequest(url: model.wikiUrl))
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(teamDidChange), name: Notification.Name(rawValue: "chageWiki"), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self)
        
    }
    
    @objc func teamDidChange(notification: Notification) {
        
        guard let userInfo = notification.userInfo else {
            return
        }
        
        var theTeam = userInfo["theTeam"] as? Team
        model = theTeam!
        syncronizeModel(model: model)
    }
    

}

extension WikiViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        spinner.isHidden = true
        spinner.stopAnimating()
    }
    
}
