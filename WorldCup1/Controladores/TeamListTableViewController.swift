//
//  TeamListTableViewController.swift
//  WorldCup1
//
//  Created by Henry AT on 9/25/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import UIKit


protocol TeamListTableViewControllerDelegate {
    
    func teamListViewController(_ viewController: TeamListTableViewController, didSelectTeam team: Team)

}

class TeamListTableViewController: UITableViewController {
    
    let model: [Team]
    var delegate: TeamListTableViewControllerDelegate?
    
    
    init(model: [Team]) {
        self.model = model
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Teams"
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.model.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellId = "cellTeam"
        
        let team = model[indexPath.row]
        
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: cellId)
        }
        
        cell?.imageView?.image = team.sigil.image
        cell?.textLabel?.text = team.name
        
        // Configure the cell...

        return cell!
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Identificar el Team
        let team = model[indexPath.row]
        
        // llama al delegate
        self.delegate?.teamListViewController(self, didSelectTeam: team)
        // Notificaciones:
        
        // Emite la notificacion
        let notificationCenter = NotificationCenter.default
        let notification = Notification(name: Notification.Name(rawValue: "chageWiki"), object: self, userInfo: ["theTeam":team ])
        notificationCenter.post(notification)
        // Persiste el dato
        saveLastSelectedTeam(position: indexPath.row)
    }
    
}

extension TeamListTableViewController {
    
    func saveLastSelectedTeam(position row: Int){
        
        let userDefault = UserDefaults.standard
        userDefault.setValue(row, forKey: "myKey")
        userDefault.synchronize()
    }
    
    func lastSelectedTeam() -> Team {
        // Extraer el row
        let row = UserDefaults.standard.integer(forKey: "myKey")
        // Averiguar el Equipo
        let team = model[row]
        // Devolver el Equipo
        return team
    }
}
