//
//  UIViewController+Additions.swift
//  WorldCup1
//
//  Created by Henry AT on 7/10/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func wrappedBNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}
