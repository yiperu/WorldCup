//
//  AppDelegate.swift
//  WorldCup1
//
//  Created by Henry AT on 6/7/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.backgroundColor = .cyan
        window?.makeKeyAndVisible()

        
        // Crear nuestros Modelos
        let teams = Repository.local.teams
        

        
        let teamListTableViewController = TeamListTableViewController(model: teams)
        // Obtenemos el ultimo equipo
        let teamSelected = teamListTableViewController.lastSelectedTeam()
        
        let teamDetailViewController = TeamDetailViewController(model: teamSelected)
        
        teamListTableViewController.delegate = teamDetailViewController
        
        let uiSplitViewControler = UISplitViewController()
        uiSplitViewControler.viewControllers = [teamListTableViewController.wrappedBNavigation(), teamDetailViewController.wrappedBNavigation()]
        
        // Establecer como Root

        let root = uiSplitViewControler
        window?.rootViewController = root
        
        return true
    }



}

