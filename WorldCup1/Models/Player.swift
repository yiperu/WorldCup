//
//  Players.swift
//  WorldCup1
//
//  Created by Henry AT on 6/7/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import Foundation

final class Player {
    
    let name: String
    let team: Team
    
    private var _nickName: String?
    
    var nickName: String {
                
        return _nickName ?? ""
    }
    
    init(name: String, nickName: String? = nil, team: Team) {
        self.name = name
        self._nickName = nickName
        self.team = team
    }
    
}

extension Player {
    var fullName: String {
        return "\(name) \(team.name)"
    }
}

extension Player {
    
    var proxyForEquatable: String {
        return "\(name) \(nickName) \(team.name)"
    }
    
    var proxyForComparable: String {
        return fullName
    }
    
}

extension Player: Hashable {
    
    var hashValue: Int {
        return proxyForEquatable.hashValue
    }
}

extension Player: Equatable {
    static func == (lhs: Player, rhs: Player) -> Bool {
        return lhs.proxyForEquatable == rhs.proxyForEquatable
    }
}

extension Player: Comparable {
    
    static func < (lhs: Player, rhs: Player) -> Bool {
        return lhs.proxyForComparable < rhs.proxyForComparable
    }
}






