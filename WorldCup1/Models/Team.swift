//
//  File.swift
//  WorldCup1
//
//  Created by Henry AT on 6/7/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import Foundation

typealias Word = String
typealias Members = Set<Player>

final class Team {
    
    let name: String
    let sigil: Sigil
    let word: Word
    private var _members: Members
    let wikiUrl: URL
    
    
    init(name: String, sigil: Sigil, word: Word, url: URL) {
        self.name = name
        self.sigil = sigil
        self.word = word
        self._members = Members()
        self.wikiUrl = url
    }
}

extension Team {
    
    var count: Int {
        return _members.count
    }
    
//    func members() -> [Player] {
//        return _members
//    }
    
    func add(_ player: Player) {
        
        guard player.team.name == name else {
            return
        }
        _members.insert(player)
    }
    
    func add(players: Player...) {
        
//        for player in players {
//            add(player)
//        }
        players.forEach{ add($0) }
    }
    

    
}


extension Team {
    var proxyForEquatable: String {
        return "\(name) \(word) \(count)"
    }
    
    var proxyForComparable: String {
        return name.uppercased()
    }
}

extension Team: Equatable {
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.proxyForEquatable == rhs.proxyForEquatable
    }
}

extension Team: Comparable {
    static func < (lhs: Team, rhs: Team) -> Bool {
        return lhs.proxyForComparable < rhs.proxyForComparable
    }
}

extension Team: Hashable {
    var hashValue: Int {
        return proxyForEquatable.hashValue
    }
}

extension Team {
    
    func sortedMembers() -> [Player] {
        return _members.sorted()
    }
}

















