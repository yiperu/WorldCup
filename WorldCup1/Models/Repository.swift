//
//  Repository.swift
//  WorldCup1
//
//  Created by Henry AT on 7/10/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import Foundation
import UIKit

final class Repository {
    static let local = Localfactory()
    
}

protocol TeamFactory {
    
    typealias Filter = (Team)-> Bool
    
    var teams: [Team] { get }
    
    func team(named name: String) -> Team?
    
    func teams(filterBy: Filter) -> [Team]
}

final class Localfactory: TeamFactory {
    
    var teams: [Team] {
        
        let peruSigil = Sigil(description: "Colores rojo y blanco", image: #imageLiteral(resourceName: "peru_logo.jpg"))
        let franciaSigil = Sigil(description: "Gallo blanco", image: UIImage(named: "francia_logo.png")!)
        let argentinaSigil = Sigil(description: "Colores celeste y blanco", image: UIImage(named: "argentina_logo.png")!)
        
        
        let peruTeam = Team(name: "Peru", sigil: peruSigil, word: "Firme y feliz por la Unión", url: URL(string: "https://es.wikipedia.org/wiki/Federaci%C3%B3n_Peruana_de_F%C3%BAtbol")!)
        let franciaTeam = Team(name: "Francia", sigil: franciaSigil, word: "Liberta, igualdad y fraternidad", url: URL(string: "https://fr.wikipedia.org/wiki/%C3%89quipe_de_France_de_football")!)
        let argentinaTeam = Team(name: "Argentina", sigil: argentinaSigil, word: "En unión y libertad", url: URL(string: "https://es.wikipedia.org/wiki/Asociaci%C3%B3n_del_F%C3%BAtbol_Argentino")!)
        
        
        let paolo = Player(name: "Paolo Guerrero", nickName: "Depredador", team: peruTeam)
        let aquino = Player(name: "Pedro Aquino", team: peruTeam)
        let farfan = Player(name: "Jefferson Farfán", nickName: "foquita", team: peruTeam)
        let cueva = Player(name: "Christian Cueva", team: peruTeam)
        let edison = Player(name: "Edison Flores", nickName: "El Oreja", team: peruTeam)
        
        
        let antonie = Player(name: "Antoine Griezmann", team: franciaTeam)
        let mbape = Player(name: "Kylian Mbappé", team: franciaTeam)
        let pogba = Player(name: "Paul Pogba", team: franciaTeam)
        
        let messi = Player(name: "Lionel Messi", nickName: "la Pulga", team: argentinaTeam)
        let dimaria = Player(name: "Ángel Di María", team: argentinaTeam)
        
        peruTeam.add(paolo)
        peruTeam.add(aquino)
        peruTeam.add(farfan)
        peruTeam.add(cueva)
        peruTeam.add(edison)
        
        franciaTeam.add(antonie)
        franciaTeam.add(mbape)
        franciaTeam.add(pogba)
        
        argentinaTeam.add(messi)
        argentinaTeam.add(dimaria)
        
        return [peruTeam, franciaTeam, argentinaTeam].sorted()
    }
    
    func team(named name: String) -> Team? {
        let team = teams.filter{ $0.name.uppercased() == name.uppercased() }.first
        return team
    }
    
    func teams(filterBy: Filter) -> [Team] {
        return Repository.local.teams.filter(filterBy)
    }
    
}























