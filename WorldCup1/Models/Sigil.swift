//
//  Sigil.swift
//  WorldCup1
//
//  Created by Henry AT on 6/7/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import Foundation
import UIKit

final class Sigil {
    
    let description: String
    let image: UIImage
    
    init(description: String, image: UIImage) {
        self.description = description
        self.image = image
    }
}

