//: Playground - noun: a place where people can play

import UIKit

/*
// Funciones como valores de retorno

func adder1 (x: Int, y: Int) -> Int {
    return x + y
}

func adder0 (x: Int) -> Int {
    return x * 2
}



print(adder1(x: 5, y: 7))


typealias funcIntToInt = (Int) -> Int

func adder2 (x: Int, y: Int) -> funcIntToInt {
    
    func f(z: Int)-> Int {
        return x + z
    }
    return f
}


let funcionInterna = adder2(x: 5, y: 7)
let unEntero = funcionInterna(9)

// Funciones como parametros

func apply(f: funcIntToInt, n: Int) -> Int {
    return f(n)
}

apply(f: adder0, n: 4)

// Simplificando funcinones con $#

func funcSimplificada1 (_ x: Int) -> Int {
    return x + 1
}

let funcSimplificada2 = { (x: Int) in
    return x + 1
}

let funcSimplificada3 = { $0 + 1 }

funcSimplificada1(7)
funcSimplificada2(7)
funcSimplificada3(7)

let funciones = [funcSimplificada1, funcSimplificada2, funcSimplificada3]

for elementoFuncion in funciones {
    elementoFuncion(45)
}
*/


// map
// filter
// compactMap
// flatmap
// reduce


// Map:

/*
let events = [6, 12, 2, 8, 4, 10]

var array01 = [Int]()

for elemento in events {
    array01.append(elemento * 2)
}

print(array01)

let array02 = events.map{ $0 * 2 }.map { String($0) }
print(array02)

// filter
array01 = []
for elemento in events {
    if elemento > 6 {
        array01.append(elemento)
    }
}
print(array01)

array01 = []
array01 =  events.filter{ $0 > 6}
print(array01)

// compactMap

let eventsConNils = [5, nil, nil, 15, 20, nil,  80, 40, nil,  10]
array01 = []
array01 = eventsConNils.compactMap{ $0 }
print(array01)

let newArray = eventsConNils.compactMap{ $0 }.filter{ $0 > 10}.map{ $0 * 3 }.map{ String($0) }.map { "Hola \($0)" }
print(newArray)

*/

// reduce

let events = [1, 2, 3, 4, 5]

var result = 0
for element in events {
    result = result + element
}

print(result)

var result1 = events.reduce(0){ (sum: Int, elem: Int) in
    sum + elem
}

print(result1)

var result2 = events.reduce(0) { $0 + $1 }
print(result2)

var result3 = events.reduce(0, +)
print("Resultado 3: \(result3)")

var result4 = events.reduce(1, *)
print(result4)

// flatMap
let test = [1, 2, 3].map {
    Array(repeating: "a", count: $0)
}
print(test)

let testConFlatMap = [1, 2, 3].flatMap {
    Array(repeating: "a", count: $0)
}
print(testConFlatMap)















