//
//  RepositoryTests.swift
//  WorldCup1Tests
//
//  Created by Henry AT on 7/10/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import XCTest
@testable import WorldCup1

class RepositoryTests: XCTestCase {
    
    var localTeam: [Team]!
    
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        localTeam = Repository.local.teams
    }
    
    func testlocalRepositoryCreation() {
        
        let local = Repository.local
        XCTAssertNotNil(local)
    }
    
    func testLocalRepositoryTeamsCreation() {
        
        XCTAssertNotNil(localTeam)
        XCTAssertEqual(localTeam.count, 3)
    }

    func testLocalRepositoryReturnSortedArrayOfTeams() {
        XCTAssertEqual(localTeam, localTeam.sorted())
    }
    
    func testLocalRepositoryReturnTeamByCaseInsensitively() {
        
        let peru = Repository.local.team(named: "pEru")
        XCTAssertEqual(peru?.name, "Peru")
        
        let joedays = Repository.local.team(named: "Joedays")
        XCTAssertNil(joedays)
    }

    func testTeamFiltering() {
        
        let filtered = Repository.local.teams(filterBy: {$0.count == 2 })
        XCTAssertEqual(filtered.count, 1)
    }
    
    
}










