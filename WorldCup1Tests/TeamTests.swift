//
//  WorldCup1Tests.swift
//  WorldCup1Tests
//
//  Created by Henry AT on 6/7/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import XCTest

@testable import WorldCup1

class TeamTests: XCTestCase {
    
    var peruSigil: Sigil!
    var franciaSigil: Sigil!
    
    var peruTeam: Team!
    var franciaTeam: Team!
    
    var paolo: Player!
    var aquino: Player!
    
    var antonie: Player!
    var mbape: Player!
    
    
    override func setUp() {
        super.setUp()
        
        peruSigil = Sigil(description: "Colores rojo y blanco", image: UIImage())
        franciaSigil = Sigil(description: "Gallo blanco", image: UIImage())
        
        peruTeam = Team(name: "Peru", sigil: peruSigil, word: "Firme y feliz por la Unión", url: URL(string: "https://es.wikipedia.org/wiki/Federaci%C3%B3n_Peruana_de_F%C3%BAtbol")!)
        franciaTeam = Team(name: "Francia", sigil: franciaSigil, word: "Liberta, igualdad y fraternidad", url: URL(string: "https://fr.wikipedia.org/wiki/%C3%89quipe_de_France_de_football")!)
        
        paolo = Player(name: "Paolo Guerrero", nickName: "Depredador", team: peruTeam)
        aquino = Player(name: "Pedro Aquino", team: peruTeam)
        
        antonie = Player(name: "Antoine Griezmann", team: franciaTeam)
        mbape = Player(name: "Kylian Mbappé", team: franciaTeam)
        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testTeamExistense() {
        
        XCTAssertNotNil(peruTeam, "Estsa instancia no el nil")
    }
    
    func testSigilExistence() {
        
        XCTAssertNotNil(peruSigil)
        XCTAssertNotNil(franciaSigil)
    }
    
    func testAddPlayer() {
        
        XCTAssertEqual(peruTeam.count, 0)
        
        peruTeam.add(paolo)
        XCTAssertEqual(peruTeam.count, 1)
        
        peruTeam.add(aquino)
        XCTAssertEqual(peruTeam.count, 2)

        // Valida q no exista duplicados
        peruTeam.add(paolo)
        XCTAssertEqual(peruTeam.count, 2)
        
        // Valida que no ingresa jugador de otro equipo
        peruTeam.add(antonie)
        XCTAssertEqual(peruTeam.count, 2)
    }

    
    func testAddPlayes()  {
        
        franciaTeam.add(players: antonie, mbape)
        XCTAssertEqual(franciaTeam.count, 2)
        
    }
    
    
    
}
