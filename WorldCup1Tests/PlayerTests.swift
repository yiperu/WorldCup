//
//  PlayerTests.swift
//  WorldCup1Tests
//
//  Created by Henry AT on 6/7/18.
//  Copyright © 2018 Henry AT. All rights reserved.
//

import XCTest

@testable import WorldCup1

class PlayerTests: XCTestCase {
    
    var peruSigil: Sigil!
    var franciaSigil: Sigil!
    
    var peruTeam: Team!
    var franciaTeam: Team!
    
    var paolo: Player!
    var aquino: Player!
    
    var antonie: Player!
    
    override func setUp() {
        super.setUp()
        
        peruSigil = Sigil(description: "Colores rojo y blanco", image: UIImage())
        franciaSigil = Sigil(description: "Gallo blanco", image: UIImage())
        
        peruTeam = Team(name: "Peru", sigil: peruSigil, word: "Firme y feliz por la Unión", url: URL(string: "https://fr.wikipedia.org/wiki/%C3%89quipe_de_France_de_football")!)
        franciaTeam = Team(name: "Francia", sigil: franciaSigil, word: "Liberta, igualdad y fraternidad", url: URL(string: "https://fr.wikipedia.org/wiki/%C3%89quipe_de_France_de_football")!)
        
        paolo = Player(name: "Paolo Guerrero", nickName: "Depredador", team: peruTeam)
        aquino = Player(name: "Pedro Aquino", team: peruTeam)
        
        antonie = Player(name: "Antoine Griezmann", team: franciaTeam)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExistencePlayer() {
        
        XCTAssertNotNil(paolo)
        XCTAssertNotNil(aquino)
    }
    
    func testFullName() {
        XCTAssertEqual(paolo.fullName, "Paolo Guerrero Peru")
        
        XCTAssertEqual(antonie.fullName, "Antoine Griezmann Francia")
    }

    
}
